UP = "UP"
DOWN = "DOWN"
RIGHT = "RIGHT"
LEFT = "LEFT"

function main()
    local x = 64
    local y = 5
    local z = 64

    for current_z = 0, z do
        for current_y = 0, y - 1 do
            for current_x = 0, x do
                if turtle.detect() then
                    turtle.dig()
                end
                turtle.forward()
                check_fuel()
            end
            if current_y < y - 1 then
                if turtle.detectUp() then
                    turtle.digUp()
                end
                turtle.up()
                turtle.turnLeft()
                turtle.turnLeft()
            end
        end

        for _ = 1, y - 1 do
            turtle.down()
        end

        if y % 2 == 0 then
            changeLine(RIGHT)
        else
            if current_z % 2 == 0 then
                changeLine(LEFT)
            else
                changeLine(RIGHT)
            end
        end
    end
end

function changeLine(direction)
    turn(direction)
    if turtle.detect() then
        turtle.dig()
    end
    turtle.forward()
    turn(direction)
end

function turn(direction)
    if direction == UP then
        turtle.up()
    elseif direction == DOWN then
        turtle.down()
    elseif direction == RIGHT then
        turtle.turnRight()
    elseif direction == LEFT then
        turtle.turnLeft()
    else
        print("Wrong direction")
    end
end

function check_fuel()
    local fuel = turtle.getFuelLevel()
    if fuel < 100
    then
        turtle.refuel(1)
    end
end

main()
